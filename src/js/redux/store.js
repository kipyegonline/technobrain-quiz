import { createStore } from "redux";
import RootReducer from "./reducer"
const devtools = window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : f => f;

export const store = createStore(RootReducer, devtools);