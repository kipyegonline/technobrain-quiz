import React from "react"
import styled, { createGlobalStyle, css } from "styled-components"
import PropTypes from "prop-types"
import { store } from "../../redux/store"
import { Input } from "./Menu"
const stateChecker3 = (c, id, name) => {

    const res = c.find(as => as['name'] === id && as['menuName'] == name)

    return res ? true : false;
}
const RelatedDiv = styled.div`
margin:.25rem 3rem;
display:${props => props.display};
    
; `
const Para = styled.p`
font - weight: bold;
margin: 0 0 0 1rem;
text-align: center; `
const MoreRelatedDiv = styled.div`
margin: .25rem 3rem;
display:${props => props.display};`

function BottomMenu({ related, name, showMore, relatedMenuState }) {

    const handleChange = (e, related, menuName) => {
        if (e.target.checked) {
            store.dispatch({
                type: "ADD_OTHERS",
                payload: { ...related, menuName }
            })
        }
        else {
            store.dispatch({
                type: "REMOVE_OTHERS",
                payload: related.name
            })
        }
    };
    const handleExtra = (e, extraRelated, menuName, submenuName) => {
        if (e.target.checked) {
            store.dispatch({
                type: "ADD_EXTRA",
                payload: { menuName, submenuName, ...extraRelated, checked: true }
            });
        }
        else {
            store.dispatch({
                type: "REMOVE_EXTRA",
                payload: extraRelated.name
            });
        }
    }



    return (
        <RelatedDiv display={showMore}>
            <Para>You might also want.</Para>
            {related.map((item, index) => {

                const innerChoices = item.choices;
                return (
                    <div key={index}>
                        <RelatedItem
                            key={index}
                            hc={(e) => handleChange(e, item, name)}
                            {...item} />
                        <MoreRelatedDiv
                            display={stateChecker3(relatedMenuState, item.name, name) ? "block" : "none"}>

                            {innerChoices.map((inner, index) => <RelatedItem
                                key={index}
                                hc={(e) => handleExtra(e, inner, name, item.name)}
                                {...inner}
                            />
                            )}


                        </MoreRelatedDiv>


                    </div>)
            })





            }

        </RelatedDiv >
    )

}
const RelatedItem = ({ name, hc, style }) => (
    <label style={{ display: "block" }}>
        <Input style={style} onChange={hc} />{name}

    </label>);
export default BottomMenu;
BottomMenu.propTypes = {
    display: PropTypes.bool,
    related: PropTypes.array,
    hc: PropTypes.func
}
RelatedItem.propTypes = {
    name: PropTypes.string,
    hc: PropTypes.func,
    style: PropTypes.object
}