import React, { useState } from "react";
import styled, { createGlobalStyle } from "styled-components"
import PropTypes, { checkPropTypes } from "prop-types"
import { Input } from "./Menu"
import { store } from "../../redux/store"
const ChoicesDiv = styled.div`
margin:.5rem 2rem;
background:#fefefe;
display:${props => props.display};`


function MiddleMenu({ choices, children, display, menuType }) {

    const handleChoices = (e, choice, menuName) => {
        if (e.target.checked) {

            const payload = { ...choice, menuName, checked: true }
            store.dispatch({
                type: "ADD_MID",
                payload
            })
        } else {

            store.dispatch({
                type: "REMOVE_MID",
                payload: choice.name,
            })
        }

    }
    return (
        <ChoicesDiv display={display}>
            {choices.map((choice, i) => <Choice key={i} hd={(e) => handleChoices(e, choice, menuType)} {...choice} />)}
            {children}
        </ChoicesDiv>)

}

const Choice = ({ name, hd }) => (
    <label style={{ display: "block" }}>
        <Input onChange={hd} />{name}

    </label>)
export default MiddleMenu;
MiddleMenu.propTypes = {
    choices: PropTypes.array,
    hd: PropTypes.func,
    menuType: PropTypes.string,
    display: PropTypes.string,
    children: PropTypes.node
}