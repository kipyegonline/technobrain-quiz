import React, { useState } from "react"
import PropTypes, { checkPropTypes } from "prop-types"
import { Input } from "./Menu"
function TopMenu({ name, hc, checkedInput, classlist, children, }) {

    return (
        <>            <label style={{ display: "block" }}>
            <Input onChange={hc} value={name} checked={checkedInput} className={classlist} />{name}</label>

            {children}


        </>

    )
}
export default TopMenu;
TopMenu.propTypes = {
    name: PropTypes.string,
    hc: PropTypes.func,
    children: PropTypes.node
}

export const Link = ({ href, text }) => {
    const handleCLick = () => console.log("click")
    return <a href={href} onClick={handleCLick}>{text}</a>
}