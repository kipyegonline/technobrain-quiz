import { C } from "./types"
const initState = {
    topMenu: [],
    midMenu: [],
    others: [],
    extra: []
}

function RootReducer(state = initState, action) {
    switch (action.type) {
        case C.ADD_TOP:
            return {
                ...state,
                topMenu: [...state.topMenu, action.payload]
            }

            break;
        case C.REMOVE_TOP:
            return {
                ...state,
                topMenu: state.topMenu.filter(men => men.name !== action.payload.name),

            }
        case C.ADD_MID:
            return {
                ...state,
                midMenu: [...state.midMenu, action.payload]
            }

        case C.REMOVE_MID:
            return {
                ...state,
                midMenu: state.midMenu.filter(men => men.name !== action.payload)
            }
        case C.ADD_OTHERS:
            return {
                ...state,
                others: [...state.others, action.payload]
            }
            break;
        case C.REMOVE_OTHERS:
            return {
                ...state,
                others: state.others.filter(men => men.name !== action.payload)
            }
            break;
        case C.ADD_EXTRA:
            return {
                ...state,
                extra: [...state.extra, action.payload]
            }
            break;
        case C.REMOVE_EXTRA:
            return {
                ...state,
                extra: state.extra.filter(men => men.name !== action.payload)
            }
            break;


        default:
            return state;
    }
}
export default RootReducer;