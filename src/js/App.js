import React from "react";
import ReactDOM from "react-dom";
import styled, { createGlobalStyle } from "styled-components";
import PropTypes from "prop-types"
import { Provider } from "react-redux"
import { store } from "./redux/store"
import { data } from "./lib/data";
import RestaurantMenu from "./components/menu/Menu";
import { checkPropTypes } from "prop-types";

//Global css for body, main div and header using styled components
const Body = createGlobalStyle`
body{
    max-width:1000px;
    margin:1rem auto;
    padding:1rem;
    font-family:helvetica;
    font-size:1rem;
    line-height:1em;
    background:#ccc;}
`
const MainDiv = styled.div`
background:#ddd;
padding:1rem;`

const Header = styled.h3`
text-align:center;
font-family:raleway;`
const SVG = styled.svg`
width:500px;
height:250px;
background:palevioletred;
margin:2rem auto;
`
const G = styled.g`
transform:translate(0,${props => props.y})`

//render the main app component with redux store by wrapping Redux provider
function App({ data }) {

    return (
        <Provider store={store}>
            <Body />
            <MainDiv>
                <Header>Restaurant Menu</Header>
                <RestaurantMenu data={data} />


            </MainDiv>
        </Provider>
    )

}
//render app to the DOM
ReactDOM.render(<App data={data} />, document.getElementById("root"))
//validating props
App.propTypes = {
    data: PropTypes.array.isRequired,
    store: PropTypes.object.isRequired
}