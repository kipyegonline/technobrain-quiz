import React, { useState } from "react";
import { connect } from "react-redux";
import PropTypes, { checkPropTypes } from "prop-types"
import styled from "styled-components";
import { store } from "../../redux/store";
import TopMenu from "./TopMenu";
import MiddleMenu from "./MiddleMenu";
import BottomMenu from "./BottomMenu";

//helper function for checking when top menu is opened or collapsed
const stateChecker = (a, id) => {
    if (a.length > 0) {
        console.log('parent name and', a, id)
        const res = a.find(as => as['name'] === id)
        return res ? true : false;
    } else {
        return false
    }

}


//helper function for checking when mid  menu is opened or collapsed
const stateChecker2 = (b, id) => {

    let res = {}
    b.find(as => {
        if (as['menuName'] === id) {
            res.display = true

        } else {
            res.display = false
        }


    })
    const absa = b.every(bas => bas.menuName === id)

    return res
}

//checkbox input for all inputs and styled css for our divs
export const Input = styled.input.attrs((props) => ({
    type: "checkbox",
    checked: props.checkedInput,
    onChange: props.onChange,
    value: props.value,
    className: props.classlist


}))`
padding:1rem;
display:inline-block;
margin: .5rem 1rem;

height:20px;
`
export const TextInput = styled.input.attrs(props => (
    {
        type: "text",
        placeholder: props.placeholder,
        onChange: props.onChange,
        value: props.value,
        className: props.classlist
    }))`
 padding:1rem;
 font-size:10px;
`


export const InputD = ({ onChange, checkedInput }) => (<input onChange={onChange} checked={checkedInput} type="checkbox" />)

//inline style  for main div
const MainMenuDiv = styled.div`
margin:0 1rem;
background:white;
display:none;`


//style the main container using inline css
const mainDiv = {
    margin: ".5rem auto",
    border: "1px solid yellow",
    borderRadius: 10,
    padding: 16,
    width: 500,
}



export function RestaurantMenu({ data, topMenu, relatedMenuState, midMenu }) {

    //handle when salad,soup or entree checkbox is clicked
    const handleMainMenu = (e, menu) => {

        if (e.target.checked) {
            //if checked send state to redux store
            store.dispatch({
                type: "ADD_TOP",
                payload: { ...menu, checked: true }
            });


        } else {
            //if checked send state to redux store so we can collapse the menu
            store.dispatch({
                type: "REMOVE_TOP",
                payload: menu.name
            });
        }
    }


    console.log(data)



    return (
        <>
            <div style={mainDiv}>
                {data.map((menu, i) => {
                    //get the middle menu and related menu
                    const choices = menu.choices;
                    const related = menu.related;
                    // ensure that top menu checkbox is unchecked at first
                    menu.checked = false
                    return (

                        <TopMenu key={i}
                            name={menu.name}
                            checkedInput={menu.checked}

                            hc={(e) => handleMainMenu(e, menu)}
                        >
                            {/**mapping through data rendering soup,salad and rentre , pass props to input and render its children, choices and related */}

                            <MiddleMenu
                                choices={choices}

                                display={stateChecker(topMenu, menu.name) ? "block" : "none"}

                                checkedInput={stateChecker(midMenu, menu.name)}
                                menuType={menu.name}
                            >
                                {/*Rendering choices after loopinhg it inside main menu */}
                                {related.length > 0 ? <BottomMenu
                                    related={related}
                                    name={menu.name}
                                    relatedMenuState={relatedMenuState}
                                    showMore={stateChecker2(midMenu, menu.name).display ? "block" : "none"}
                                    checkedInput={stateChecker2(midMenu, menu.name)}
                                /> : null}
                                {/*Checking if an array of what a user might be Rendering you might be interested inside choices menu */}
                            </MiddleMenu>
                        </TopMenu>
                    )
                })}

            </div>
        </>
    )
}



//connecting state from redux store to react
const mapStateToProps = (state) => ({
    topMenu: state.topMenu,
    relatedMenuState: state.others,
    midMenu: state.midMenu,
    others: state.others
})
export default connect(mapStateToProps)(RestaurantMenu);

//validating props
RestaurantMenu.propTypes = {
    data: PropTypes.array.isRequired,
    topMenu: PropTypes.array.isRequired,
    relatedMenuState: PropTypes.array.isRequired,
    midMenu: PropTypes.array.isRequired,
    others: PropTypes.array,
    hc: PropTypes.func,
    showMore: PropTypes.bool,
    menuType: PropTypes.string,
    choices: PropTypes.array,
    related: PropTypes.array,
    children: PropTypes.node
}