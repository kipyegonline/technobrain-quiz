import React from "react";
import Enzyme, { shallow, mount } from "enzyme"
import Adapter from "enzyme-adapter-react-16"
import { compose } from "react-redux"
import toJSON from "enzyme-to-json"
Enzyme.configure({ adapter: new Adapter() })
import TopMenu from "../components/menu/TopMenu"
import { RestaurantMenu, Input } from "../components/menu/Menu"


describe("<TopMenu/>", () => {
    let wrapper
    let _change = jest.fn();
    beforeAll(() => {

        wrapper = shallow(<TopMenu hc={_change}
            children={Others}
            checkedInput={false}
            classlist="Vince" />)
        wrapper.setProps({ name: "Jules" })
        //console.log(wrapper.debug())
    })
    it("renders and matches snapshot", () => {
        shallow(<TopMenu />)
        expect(toJSON(wrapper)).toMatchSnapshot()
    })

    test("presence of wrapper and matches snapshot", () => {

        expect(wrapper.find(Input)).toHaveLength(1)
        expect(wrapper.find(Input).prop("checked")).toBe(false)
        wrapper.find(Input).simulate("change", { target: { checked: true } })
        wrapper.update()
        wrapper.setProps({ checkedInput: true })
        expect(wrapper.find(Input).prop("checked")).toBe(true)
        expect(_change).toHaveBeenCalled()
        expect(wrapper.find(".Vince")).toHaveLength(1)



        // expect(wrapper.prop("checked")).toBe(false)
        expect(wrapper.find('p').text()).toEqual("Just mock it")
    })

})
const Others = <div><p>Just mock it</p></div>
const data = [{
    name: "JUles",
    choices: [{ name: "Sheila", name: "Mark", choices: [{ name: "Ben", name: "Kev" }] }, { name: "Ben", name: "Kev" }, { name: "Allan", name: "June" },],
    related: [{ name: "Sheila", name: "Mark", choices: [{ name: "Ben", name: "Kev" }] },
    { name: "Ben", name: "Kev", choices: [{ name: "Ben", name: "Kev" }] },
    { name: "Allan", name: "June", choices: [] },]
}]

describe("<Menu/>", () => {
    let wrapper = mount(<RestaurantMenu

        data={data}
        topMenu={[]}
        relatedMenuState={[]}
        midMenu={[]} />)
    console.log(wrapper.debug())
    it("matches snapshot", () => {
        expect(toJSON(wrapper)).toMatchSnapshot()
    })
})