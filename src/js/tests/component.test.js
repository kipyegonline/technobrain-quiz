import { store } from "../redux/store"
import RootReducer from "../redux/reducer"

//before all tests run
beforeAll(() => {
    store.dispatch({ type: "ADD_TOP", payload: { name: "Vince" } })
    store.dispatch({ type: "ADD_TOP", payload: { name: "Vince" } })
})
//after all tests have ran
afterAll(() => {
    store.dispatch({ type: "REMOVE_TOP", name: "VINCE" })
    store.dispatch({ type: "REMOVE_TOP", name: "VINCE" })
    test('after all have gone', () => {
        expect(store.getState().topMenu.length).toBeGreaterThan(2);
    })

})

//testing reducers
describe("Reducers", () => {
    const state = { topMenu: [] }, action = { type: "ADD_TOP", payload: { name: "Vince" } }
    const res = RootReducer(state, action)
    test('state', () => {
        expect(res).toEqual({ topMenu: [{ name: "Vince" }] })
    })

    test('Removing top', () => {

        let st = { topMenu: [{ name: "Vince" }] }, actio = { type: "REMOVE_TOP", payload: "Vince" };
        const r = RootReducer(st, actio);
        expect(r).toEqual({ topMenu: [] })
    })
    test('initial state of store', () => {
        expect(RootReducer(undefined, {})).toEqual({
            topMenu: [],
            midMenu: [],
            others: [],
            extra: []
        })
    })
    test('add top menu', () => expect(store.getState().topMenu.length).toBe(2))
    test('add top menu hh', () => expect(store.getState().topMenu.name).not.toBeDefined())


})

