

### Technobrain quiz challenge for Privacy Center Web Dev ###
The project is available on bitbucket 

# Quick summary
   # The first question on MYSQL  was sent file.txt on email

   # The second question is attached on SRC folder
   I attempted the question in Javascropt language

   # The third question menu Restaurant quiz is attached on SRC  menu folder
I used react js,styled components and redux to set up the project.

React js assisted in reusing  the menu components and updating the state.I used react hooks, functional react, to create components as the project is small and manageable.

Redux helped in  managing the data in handing changes in the checkbox and managing the opening and collapsing of the main menu and the nested menus. Each menu dispatched an action to the store where I could check which button was clicked.Redux will make it easy to manage the checkboxes after the user opts out.

I used Styled components, css in js library, to style the components since this is a small project.


### Set Up ###
The app can be initialized on package.json by running the scripts object
The code base is on the SRC folder, the js folder is where the react componmets and inline css are located.
The app  module is bundled by webpack, App.js in src/js is the entry point of webpack and the dist folder is the build folder.

### dependencies
The project  dependencies include: react js,redux,react redux, reactDOM styled.
Webpack, eslint and babel are the dev dependencies for set up.


### TESTS
The tests can be  run on the terminal; npm test
The unit tess were managed by jest and enzyme. 
### DEPLOYMENT
Run "npm run build" on the terminal to generate the build project


